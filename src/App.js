import React from "react";
import logo from "./gitlab.svg";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <a
          className="App-link"
          href="https://gitlab.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn GitLab CI
        </a>
      </header>
      <div className="participants">
        <h3>
          Add your name to the list to demonstrate you have completed the
          course.
        </h3>
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Username</th>
              <th>Location</th>
              <th>Message</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Chahat fateh ali khan</td>
              <td>@salamaleyykumm</td>
              <td>mars</td>
              <td>yeh jo pyara ci/cd hy.....</td>
            </tr>
            <tr>
              <td>Faizan</td>
              <td>@designbyfaizi</td>
              <td>outworld</td>
              <td>naniiii, omaiwa, moe shinde iru.....</td>
            </tr>
            <tr>
              <td>Usman</td>
              <td>@usman</td>
              <td>Pakistan 🇵🇰</td>
              <td>maja nahi aaya bhaaya</td>
            </tr>

            <tr>
              <td>daniyal</td>
              <td>@daniyal</td>
              <td>Pakistan 🇵🇰</td>
              <td>lost in space</td>
            </tr>

            <tr>
              <td>Shaheryar</td>
              <td>@shaheryarthedev</td>
              <td>Pakistan 🇵🇰</td>
              <td>Thank you for this amazing course, great</td>
            </tr>
            <tr>
              <td>Valentin</td>
              <td>@vdespa</td>
              <td>Romania 🇷🇴</td>
              <td>Thank you for taking this course.</td>
            </tr>

            <tr>
              <td>GhislainAdon</td>
              <td>@yapo.adon@88463114</td>
              <td>Ivory coast West Africa</td>
              <td>Thank you for taking this course. It helped me a lot </td>
            </tr>
            <tr>
              <td>Prateek</td>
              <td>@prtkp93</td>
              <td>India IN</td>
              <td>
                Thank you for creating this amazing course. I love the way you
                teach and cover the content.
              </td>
            </tr>
            <tr>
              <td>Eysa</td>
              <td>@eyxana</td>
              <td>Hungary</td>
              <td>Thank you for this amazing course.</td>
            </tr>
            <tr>
              <td>Owais</td>
              <td>@owais.ibrahim</td>
              <td>Pakistan 🇵🇰</td>
              <td>Thank you for this amazing course.</td>
            </tr>
            <tr>
              <td>Albert</td>
              <td></td>
              <td>Taiwan 🇹🇼</td>
              <td>Thank you for this amazing course.</td>
            </tr>
            <tr>
              <td>Nermin</td>
              <td>@nermin.hrustic</td>
              <td>Bosnia and Herzegovina &#x1F1E7;&#x1F1E6;</td>
              <td>Thank you. I enjoyed it. A great way of teaching.</td>
            </tr>
            <tr>
              <td>Moses</td>
              <td>@moomagit</td>
              <td>Canada</td>
              <td>
                Thank you Valentin for this learning opportunity and for being
                such an excellent, passionate and selfless teacher. I have
                learnt alot. Looking forward to other opportunities to
                collaborate and learn from you
              </td>
            </tr>
            <tr>
              <td>Anurag</td>
              <td>@budme</td>
              <td>USA</td>
              <td>Thank you for this amazing course.</td>
            </tr>
            <tr>
              <td>Saugata Bhattacharyya</td>
              <td>@saubhatta</td>
              <td>India IN</td>
              <td>Thank you for making this amazing course.</td>
            </tr>
            <tr>
              <td>Mohamed eleraky</td>
              <td>@mohamedzakigithub</td>
              <td>Australia</td>
              <td>Amazing course. Thank you so much.</td>
            </tr>
            <tr>
              <td>Luiz Ricardo de Lima</td>
              <td>ricardolima</td>
              <td>Brazil 🇧🇷</td>
              <td>
                It was an amazing course full of new contents. I realize that
                there is A LOT to learn in this field.
              </td>
            </tr>
            <tr>
              <td>Beatriz de Brito</td>
              <td>@bedebrito.web</td>
              <td>Brazil 🇧🇷</td>
              <td>
                Thank you for the course, it was a great introduction to DevOps.
              </td>
            </tr>
            <tr>
              <td>Sunday Emmanuel</td>
              <td>@Okoriko1</td>
              <td>Canada🇨🇦</td>
              <td>
                Thank you for this amazing course material, it is indeed a great
                head-start for both beginners and professionals alike
              </td>
            </tr>
            <tr>
              <td>Don Ludemann</td>
              <td>@don803</td>
              <td>Kansas, USA</td>
              <td>
                Thank you for the course material. <br />
                Completed: 2022-08-14
              </td>
            </tr>
            <tr>
              <td>Sunil Bhuyan</td>
              <td>@sunil.bhuyan</td>
              <td>Odisha, India 🇮🇳</td>
              <td>The course is very much helpful and usefull </td>
            </tr>
            <tr>
              <td>Todd Seesz</td>
              <td>@toddseesz</td>
              <td>United States 🇺🇸</td>
              <td>Great course and teaching style </td>
            </tr>
            <tr>
              <td>Yovraj</td>
              <td>yuvv07</td>
              <td>India</td>
              <td>
                THANKYOU!! it was great learning from you uh made it easy for
                real
              </td>
            </tr>
            <tr>
              <td>Amit Mahato</td>
              <td>@Amitmahato</td>
              <td>Nepal 🇳🇵</td>
              <td>
                Thank you for making such an awesome course. I wanted to start
                with the DevOps but could not find where to start from. The CICD
                portion was the one that as a developer I have seen being used
                and hence was curios to learn that but I was not sure if I can
                jump directly on to that. This course has taught me lots of
                things and I guess this is the first milestone in the path to
                being a DevOps engineer that I have achieved. Thank you again,
                Valentin for making such an easy to follow course.
              </td>
            </tr>
            <tr>
              <td>Ramazan</td>
              <td>@rmzturkmen</td>
              <td>Nederlands</td>
              <td>
                It was really a step-by-step, well-prepared and explained
                course. Thank you Valentin!.
              </td>
            </tr>
            <tr>
              <td>Tong</td>
              <td>@tong-gg</td>
              <td>Thailand</td>
              <td>
                This course is helpful! Thank you so much Valentin! for making
                an amazing course, this helps me get through my senior project!
              </td>
            </tr>
            <tr>
              <td>Chris Williams</td>
              <td>@mistwire</td>
              <td>🌎</td>
              <td>Thank you for making this course ❤️🥰</td>
            </tr>
            <tr>
              <td>Allam Galan</td>
              <td>@allamgr</td>
              <td>Dominican Republic</td>
              <td>
                Awesome step-by-step I am really gratefull, I know fell a lot
                more confident with CI/CD.
              </td>
            </tr>
            <tr>
              <td>Bamba Deme</td>
              <td>@BambaDeme</td>
              <td>Dakar</td>
              <td>
                Think you! helped me understand gitlab and CI/CD! Exactly what i
                needed
              </td>
            </tr>
            <tr>
              <td>Noelle</td>
              <td>@NoelleinMN</td>
              <td>United States 🇺🇸</td>
              <td>Learned so much! Thank you, Valentin!</td>
            </tr>
            <tr>
              <td>Adrian</td>
              <td>@popaadrianvd</td>
              <td>Romania 🇷🇴</td>
              <td>
                Thank you Valentin! This was a great tutorial, I know feel a lot
                more confident working with CI/CD pipelines.
              </td>
            </tr>
            <tr>
              <td>Veneselin</td>
              <td>@veneselin</td>
              <td>Bulgaria</td>
              <td>Thank you!</td>
            </tr>
            <tr>
              <td>Aleksandr</td>
              <td>@abolychev</td>
              <td>Russia</td>
              <td>Thanks for the course!</td>
            </tr>
            <tr>
              <td>Fatih</td>
              <td>@fatihtepe</td>
              <td>Canada🇨🇦</td>
              <td>Thank you, this course was an amazing experience </td>
            </tr>
            <tr>
              <td>Ibrahima </td>
              <td>@biranbirane1210</td>
              <td>Sénégal🇸🇳 </td>
              <td>
                It was exciting and very interesting. Thank you 🙏 for this
                great content{" "}
              </td>
            </tr>
            <tr>
              <td>Jules</td>
              <td>@julesyoum</td>
              <td>Sénégal 🇸🇳 </td>
              <td>Very interesting content. Thank you very much </td>
            </tr>
            <tr>
              <td>Jamie</td>
              <td>@JAZ013</td>
              <td>Australia</td>
              <td>
                Fantastic course. I have learned so much that I have already
                started using in my day-to-day work. Thank you!
              </td>
            </tr>
            <tr>
              <td>Jesse</td>
              <td>@jpadillaa</td>
              <td>Colombia 🇨o</td>
              <td>Thank you, Valentine</td>
            </tr>
            <tr>
              <td>JBuck</td>
              <td>@buckdevment</td>
              <td>Cuba 🇨🇺</td>
              <td>
                Valentine for president! This course creates better citizens!!
              </td>
            </tr>
            <tr>
              <td>Anna</td>
              <td>@aakojyan</td>
              <td>Armenia</td>
              <td>This was a great and very informative course. Thank you!</td>
            </tr>
            <tr>
              <td>Billy</td>
              <td>@BillyGrande</td>
              <td>Greece</td>
              <td>
                Very helpful course! Thank you for providing us such resources.
              </td>
            </tr>
            <tr>
              <td>Vinícius</td>
              <td>@bl4cktux89</td>
              <td>Brazil 🇧🇷</td>
              <td>Your coarse is amazing and i learned a lot!</td>
            </tr>
            <tr>
              <td>Pedro</td>
              <td>@pedro.calvo</td>
              <td>Mexico 🇲🇽</td>
              <td>
                Excellent course! Amazing to learn automation pipelines, gitlab
                CI/CD and Devops cycle
              </td>
            </tr>
            <tr>
              <td>Innocent Onyemaenu</td>
              <td>@rockcoolsaint</td>
              <td>Nigeria 🇳🇬</td>
              <td>
                Thank you for taking your time to create this wonderful and easy
                to follow along course.
              </td>
            </tr>
            <tr>
              <td>Choustoulakis Nikolaos</td>
              <td>@NikosChou</td>
              <td>Greece 🇬🇷</td>
              <td>Thank you for sharing!</td>
            </tr>
            <tr>
              <td>William F. Silva</td>
              <td>@willianfrancas</td>
              <td>Brazil 🇧🇷</td>
              <td>
                For a long time I was looking for a content to learn about
                DevOps CI/CD, and your course from freecodecamp youtube channel,
                helped me a lot! I am working as front end developer. Now I have
                more than I need to delivery content at work or in side
                projects. I will let others people to know about you! Thank you
                so much, Despa! You deserve more than words!
              </td>
            </tr>
            <tr>
              <td>Muhammad Abutahir</td>
              <td>@abutahir</td>
              <td>India</td>
              <td>
                This is a must watch and implement course for every developer!
                Thank you so much 💙.
              </td>
            </tr>
            <tr>
              <td>KagemniKarimu</td>
              <td>@KagemniKarimu</td>
              <td>USA (Afrikan)</td>
              <td>Awesome course. I enjoyed it.</td>
            </tr>
            <tr>
              <td>Leo</td>
              <td>@leonel.barriga</td>
              <td>Argentina🇦🇷</td>
              <td>Thanks for the course Valentin. It was great!</td>
            </tr>
            <tr>
              <td>Phat NV</td>
              <td>@phat-nv</td>
              <td>Viet Nam</td>
              <td>Thank you for sharing this course.</td>
            </tr>
            <tr>
              <td>Rahul Gupta</td>
              <td>@rahulgupta141998</td>
              <td>India</td>
              <td>Thank you for the course</td>
            </tr>
            <tr>
              <td>Thomas Poth</td>
              <td>@thomas.poth</td>
              <td>Germany</td>
              <td>
                Valentin, you have made really great content. It fits perfectly
                as I currently have many GitLab projects running. One of the
                last secrets was GitLabc&apos;s CI/CD system. Thank you for this
                course!
              </td>
            </tr>
            <tr>
              <td>Santosh Dawanse</td>
              <td>@dawanse-santosh</td>
              <td>Nepal 🇳🇵</td>
              <td>Great content, Thank you for offering this course 💙.</td>
            </tr>
            <tr>
              <td>Axel Somerseth Cordova</td>
              <td>@axelsomerseth</td>
              <td>Honduras 🇭🇳</td>
              <td>Thank you for this course. I learned a lot! 🚀</td>
            </tr>
            <tr>
              <td>Eric Daly</td>
              <td>@linucksrox</td>
              <td>United States</td>
              <td>
                Thanks for this course, it helped me start making my own
                pipelines already.
              </td>
            </tr>
            <tr>
              <td>Maciej Friedel</td>
              <td>@ippolit</td>
              <td>Poland</td>
              <td>Thanks to this course I had a successful weekend</td>
            </tr>
            <tr>
              <td>Muiz Uvais</td>
              <td>@Muiz U</td>
              <td>Sri Lanka</td>
              <td>
                I found the course to be extremely useful, and informative.
                Thanks for making such great content
              </td>
            </tr>
            <tr>
              <td>Mohamed Nasrullah</td>
              <td>@nasr16</td>
              <td>India</td>
              <td>
                Really great course. Learned a lot in this one video. Thank you
                for offering this course 💙.
              </td>
            </tr>
            <tr>
              <td>Alex</td>
              <td>@AlexAg97</td>
              <td>Ecuador 🇪🇨</td>
              <td>
                I really appreciate this course, it is a light on the dark path
                of DevOps.
              </td>
            </tr>
            <tr>
              <td>Norman David</td>
              <td>@david_mbuvi</td>
              <td>Kenya</td>
              <td>
                I am really grateful for this course. Thank you for offering
                this course.
              </td>
            </tr>
            <tr>
              <td>Paul</td>
              <td>@phall659</td>
              <td>USA</td>
              <td>Great course! Thanks for taking time to put it together.</td>
            </tr>
            <tr>
              <td>Mallikarjun Reddy Dorsala</td>
              <td>@mallikarjun.reddy1</td>
              <td>India</td>
              <td>Thank you. Enjoyed your teaching</td>
            </tr>
            <tr>
              <td>Navi Singh</td>
              <td>@navisingh</td>
              <td>India</td>
              <td>
                I am very grateful. There are so many things to learn in this
                course for beginners like me. Superb instructor.
              </td>
            </tr>
            <tr>
              <td>Ezekiel Kolawole</td>
              <td>@eazybright</td>
              <td>Nigeria 🇳🇬</td>
              <td>I learnt alot from this course. Thanks so much!</td>
            </tr>
            <tr>
              <td>David</td>
              <td>@david1058</td>
              <td>Kenya 🇰🇪 / USA</td>
              <td>Alright Kubernetes, you&#39;re next!.</td>
            </tr>
            <tr>
              <td>Andrii Tugai</td>
              <td>@atugai</td>
              <td>Ukraine 🇺🇦</td>
              <td>Thank you for the course Valentin 😀 I learnt a lot</td>
            </tr>
            <tr>
              <td>Ebenezer Makinde</td>
              <td>@ebenezermakinde</td>
              <td>Nigeria 🇳🇬</td>
              <td>
                This course was just tailored to purpose. Great content. Thanks
                for this course
              </td>
            </tr>
            <tr>
              <td>Kostiantyn</td>
              <td>@kofesenko</td>
              <td>Ukraine</td>
              <td>
                Thank you for this course. I found it to be the perfect resource
                to master my knowledge of GitLab as a platform and the features
                of GitLab CI/CD in particular.
              </td>
            </tr>
            <tr>
              <td>Jose Cano</td>
              <td>@jskno</td>
              <td>Spain</td>
              <td>
                Great course. Clear explanations, straight to the point !! Keep
                teaching, please
              </td>
            </tr>
            <tr>
              <td>Tomasz Fijarczyk</td>
              <td>@t0mmili</td>
              <td>Poland 🇵🇱</td>
              <td>
                Comprehensive, pleasantly presented course. It was actually
                understandable from beginning till end. Thank you Valentin!
              </td>
            </tr>
            <tr>
              <td>Ayodele Ademeso</td>
              <td>@ayodele-spencer</td>
              <td>Nigeria 🇳🇬</td>
              <td>
                This course was precise and easy to follow from start to finish.
                I miss it already. I was also able to setup a CI for a project I
                created earlier using this course. Thank you Valentin!
              </td>
            </tr>
            <tr>
              <td>Pranav Arora</td>
              <td>@pranavarora99.pa</td>
              <td>USA</td>
              <td>
                Thank you for making this course. easy to understand and good
                amunt of hands onn learning.
              </td>
            </tr>
            <tr>
              <td>Christian Aluya</td>
              <td>@caluya</td>
              <td>United Kingdom UK</td>
              <td>
                The best course on gitlab-ci so far, you are the best. Thank you
                a million times over!
              </td>
            </tr>
            <tr>
              <td>A.Iusupov</td>
              <td>@alikyusupov</td>
              <td>Uzbekistan 🇺🇿</td>
              <td>So precious course! Thanks Valentin</td>
            </tr>
            <tr>
              <td>Patrick Santino</td>
              <td>@sotobakar</td>
              <td>Indonesia 🇮🇩</td>
              <td>
                Learnt many things in this course. Now, I am going to start my
                journey to automate my development processes. Appreciate the
                course @FCC and @Valentin.
              </td>
            </tr>
            <tr>
              <td>Ivan Chen</td>
              <td>@chen-chao-wei</td>
              <td>Taiwan 🇹🇼</td>
              <td>
                This course is easy to understand and very helpful. Thank you
                for offering this course😀
              </td>
            </tr>
            <tr>
              <td>Lilian Etchepare</td>
              <td>@letchepare</td>
              <td>France 🇫🇷</td>
              <td>
                I loved the way this course was explained. Glad it went into my
                youtube recommendations. Thanks a lot Valentin for your work.
              </td>
            </tr>
            <tr>
              <td>Sarakorn Sakol</td>
              <td>@shurricanex</td>
              <td>Cambodia</td>
              <td>
                Learnt so many stuffs I never knew before. It is a wonderful
                course, and I was enjoying learning it a lot, thanks for
                creating this such a precious course.
              </td>
            </tr>
            <tr>
              <td>Deepak</td>
              <td>@mini2dust</td>
              <td>India</td>
              <td>
                THANK YOU!! for putting this course in such easily
                understandable way, ur teach was awesome
              </td>
            </tr>
            <tr>
              <td>Chris Brook</td>
              <td>@cbrman74</td>
              <td>UK</td>
              <td>
                Brilliant course, I have learned so much. Thanks a lot Valentin.
              </td>
            </tr>
            <tr>
              <td>Gabriel Francia</td>
              <td>@cindrmon</td>
              <td>Philippines</td>
              <td>
                This has been one of the most helpful courses I have ever took
                to kickstart my DevOps path, and I thank you so much for
                providing such content! I&apos;m looking forward to using this
                with agile scrum methodologies and better project management for
                future projects, and to learn more about the world of DevOps
                with Gitlab!
              </td>
            </tr>
            <tr>
              <td>H.D</td>
              <td>@dominguesh</td>
              <td>United States 🇺🇸</td>
              <td>
                Learned so much! Thank you, Valentin! Um salve pra voce, I
                really enjoyed learning CI/CD with you, whish me luck. I am
                getting ready for a job interview! Dec 2022
              </td>
            </tr>
            <tr>
              <td>Ha Non</td>
              <td>@somruethai.o</td>
              <td>Thailand</td>
              <td>
                Thank you for making this course! You helped me a lot for my
                senior project. You are an excellent DevOps teacher.
              </td>
            </tr>
            <tr>
              <td>Kapil Bhise</td>
              <td>@bhisekapil38</td>
              <td>India</td>
              <td>
                Thanks @Valentin! Great course. I learnt a lot from this course.
              </td>
            </tr>
            <tr>
              <td>Witchaphon Saeng-aram</td>
              <td>@switchaphon</td>
              <td>Thailand</td>
              <td>
                Thanks you! @Valentin. Your course is super awesome. Even I had
                stucked the last assignment that the EB on AWS dose not running
                properly when I deploy oue React code but it was not the core of
                this course. :){" "}
              </td>
            </tr>
            <tr>
              <td>Matthew Williams-Harrison</td>
              <td>@mattwh711</td>
              <td>United Kingdom UK</td>
              <td>
                Thank you for this amazing course, I have been using the skills
                I have learnt to build my own pipelines
              </td>
            </tr>
            <tr>
              <td>Giuseppe</td>
              <td>@peppemig</td>
              <td>Italy 🇮🇹</td>
              <td>
                Thanks so much for this course! Great to get some REAL practice
                for DevOps
              </td>
            </tr>
            <tr>
              <td>Patrick Maneclang</td>
              <td>@pats25</td>
              <td>Philippines</td>
              <td>
                Thank you very much for this course I learned a lot about the
                devops process specially in ci cd part I started my devops
                journey with this course and this helped me a lot understanding
                the devops methodologies thank you very much again sir Valentine
              </td>
            </tr>
            <tr>
              <td>Dmitry Mizernik</td>
              <td>@d.mizernik</td>
              <td>Belarus</td>
              <td>Thank you for great course</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default App;
